const app = getApp();
Page({
  data: {
    tabbar:{},
  },
  onLoad: function (options) {
    wx.hideTabBar(); // 隐藏系统自带的tabBar
    app.editTabbar();
  },
  onReady: function () {

  },
  // 用户点击右上角分享
  onShareAppMessage: function () {

  }
})