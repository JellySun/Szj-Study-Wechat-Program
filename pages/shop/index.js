const app = getApp();
const shop = require('../../utils/shop.js');
Page({
  data: {
    tabbar:{},
    active:0,
    titles:[],
    imgUrls:[],
    searchInfo:'',
    shoplist:[],
  },
  onLoad: function (options) {
    wx.hideTabBar(); // 隐藏系统自带的tabBar
    app.editTabbar();
    this.setData({
      "titles": shop.content.comprehensive,
      "imgUrls": shop.content.imgUrls
    })
  },
  slectTitle(e){
    let index= e.currentTarget.dataset.index;
    this.data.titles.forEach((e,i)=>{
      if(index === 0){
        this.setData({
          "active": index,
          'titles[0].active': "title-active",
          'titles[1].active': "",
          'titles[2].active': ""
        })
      }else if(index === 1){
        this.setData({
          "active": index,
          'titles[0].active': "",
          'titles[1].active': "title-active",
          'titles[2].active': ""
        })
      }else{
        this.setData({
          "active": index,
          'titles[0].active': "",
          'titles[1].active': "",
          'titles[2].active': "title-active"
        })
      }
    })
  },
  bindKeyInput(e){
    this.setData({
      "searchInfo": e.detail.value
    })
  },
  collectTheShop(e){
    let type = e.target.dataset.type - 1;
    let id = e.target.dataset.id;
    this.data.titles[type].list.forEach((e,i)=>{
      if(e.id == id){
        let key = "titles["+type+"].list["+i+"].flag";
        let value = this.data.titles[type].list[i].flag;
        let name = !value?'收藏成功!':'取消收藏!';
        this.showToast(name);
        this.setData({
          [key]: !value
        });
      }
    })
  },
  jumpToShopDetail: function(e){
    let that = this;
    let id = e.target.dataset.id;
    let type = e.target.dataset.type;
    wx.navigateTo({
      url: '/pages/shopdetail/index?type='+type+'&id='+id
    })
  },
  showToast(name){
    wx.showToast({
      title: name,
      icon: 'none',
      duration: 1500,
      // mask: true  // 显示透明蒙层，防止触摸穿透
    }); // 弹框提示文字
  },
  onShareAppMessage: function () { // 用户点击右上角分享

  }
})