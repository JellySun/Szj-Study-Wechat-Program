//获取应用实例
const app = getApp()
Page({
  data: {
    tabbar:{},
    motto: 'world',
    userInfo: {},
    loginPadding:'',
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  jumpLogsTap: function() {//开启日志页面
    wx.navigateTo({ url: '../logs/logs' })
  },
  onLoad: function () {
    wx.hideTabBar(); // 隐藏系统自带的tabBar
    app.editTabbar();

    if (app.globalData.userInfo) {
      this.reloadUserInfo(app.globalData);
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.reloadUserInfo(res);
      }
    } else {// 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.reloadUserInfo(res);
        }
      })
    }
  },
  reloadUserInfo(data){
    this.setData({
      userInfo: data.userInfo,
      hasUserInfo: true,
      loginPadding: '10rpx 30rpx;',
      motto: app.globalData.userInfo.nickName
    })
  },
  getUserInfo: function(e) { // 授权获取用户信息
    app.globalData.userInfo = e.detail.userInfo
    this.reloadUserInfo(e.detail);
  },
  removeUserInfo(){ // 退出登陆
    app.globalData.userInfo = {};
    this.setData({
      userInfo: {},
      hasUserInfo: false,
      motto: "world",
      loginPadding:''
    })
  }
})
