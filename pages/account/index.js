const app = getApp()
Page({
  data: {
    tabbar:{},
    followed:false,
    inputValue: '',
    titlelist:[
      {id: 0,name: '收藏',src:'icon-shop-cart'},
      {id: 1,name: '信息',src:'icon-comments'},
      {id: 2,name: '邮箱',src:'icon-email'}
    ]
  },
  onLoad: function (options) {
    wx.hideTabBar(); // 隐藏系统自带的tabBar
    app.editTabbar();
  },
  bindKeyInput(e){

    this.setDataFun('inputValue', e.detail.value)
  },
  followTheUser(){
    this.setData({
      followed: !this.data.followed
    })
    if(this.data.followed){
      wx.showToast({
        title: '关注成功',
        icon: 'succes',
        duration: 1000,
        mask:true
      })
    }
  },
  setDataFun(key, value){
    this.setData({
      key: value
    })
  },
  onShareAppMessage: function () {

  }
})