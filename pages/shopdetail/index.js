let shop = require('../../utils/shop.js');
Page({
  data: {
    showDetail:true,
    showSpellBuy:false,
    shopType:'',
    shopInfo:{},
    imgUrls:[],
    titles:[
      {id:0,name:'',price:'130',amount:0,flag:false},
      {id:1,name:'',price:'110',amount:3,flag:false},
      {id:2,name:'',price:'80',amount:5,flag:false},
      {id:3,name:'',price:'50',amount:7,flag:false},
      {id:4,name:'',price:'25',amount:10,flag:false}
    ]
  },
  onLoad: function (options) {
    let id = options.id;
    let type = options.type - 1;
    if(!shop.content.comprehensive[type]){return false;}
    let list = shop.content.comprehensive[type].list;
    list.forEach((e,i)=>{
      if(e.id == id) {
        this.setData({
          "shopType":shop.content.shopType(options.type),
          "shopInfo": e,
          "imgUrls": shop.content.imgUrls
        })
      }
    })
  },
  jumpToSpellOrder(e){
    let id = e.currentTarget.dataset.id;
    wx.showToast({
      title:'该功能暂未开启,请关注后续更新!',
      icon: 'none'
    })
  },
  showDiffContent(e){
    let flag;
    let type = e.currentTarget.dataset.type;
    if(type == 1){
      flag = true;
    }else{
      flag = false;
    }
    this.setData({
      showDetail: flag,
      showSpellBuy: !flag
    })
  },
  showShareEvent(){
    wx.showShareMenu({
      // withShareTicket: true
    })
  },
  onShareAppMessage(options){
    var shareObj = {
      title: "转发的标题", // 默认是小程序的名称(可以写slogan等)
      path: '/pages/share/share', // 默认是当前页面，必须是以‘/’开头的完整路径
      imgUrl: '', //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
      success: function(res){ // 转发成功之后的回调
        console.log(options)
        if(res.errMsg == 'shareAppMessage:ok'){}
      },
      fail: function(){
        if(res.errMsg == 'shareAppMessage:fail cancel'){　　
          // 用户取消转发
        }else if(res.errMsg == 'shareAppMessage:fail'){
          // 转发失败，其中 detail message 为详细失败信息
        }
      },
    //   complete: fucntion(){
    //     // 转发结束之后的回调（转发成不成功都会执行）
    //     // 来自页面内的按钮的
    //     if( options.from == 'button' ){
    //       var eData = options.target.dataset;
    //       console.log( eData.name );
    //       // 此处可以修改 shareObj 中的内容
    //       shareObj.path = '/pages/btnname/btnname?btn_name='+eData.name;
    //     }
    //   　return shareObj;
    //   }
    }
  }
})
