#提交时转换为LF，检出时不转换 <br>
git config --global core.autocrlf input <hr>
#拒绝提交包含混合换行符的文件 <br>
git config --global core.safecrlf warn <hr>

遇到 fatal: refusing to merge unrelated histories (说明本地版本不是最新的) <br>
解决 #加上后面这个选项允许不相关历史提交 <br>
git pull origin master --allow-unrelated-histories <hr>