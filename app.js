//app.js
App({
  onLaunch: function () {
    wx.getSystemInfo({// 获取设备的信息
      success: res=> {
        this.globalData.systemInfo = res
      }
    })
    wx.hideTabBar(); // 隐藏系统自带的tabBar
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || [];
    logs.unshift(Date.now());
    wx.setStorageSync('logs', logs);

    wx.login({// 发送 res.code 到后台换取 openId, sessionKey, unionId
      success: res => {  }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {// 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  editTabbar: function() {
    let tabbar = this.globalData.tabBar;
    let currentPages = getCurrentPages();
    let _this = currentPages[currentPages.length - 1];
    let pagePath = _this.route;
    (pagePath.indexOf('/') != 0) && (pagePath = "/"+ pagePath);
    for (let i in tabbar.list) {
      tabbar.list[i].selected = false;
      (tabbar.list[i].pagePath == pagePath) && (tabbar.list[i].selected = true);
    }
    _this.setData({
      tabbar: tabbar
    });
  },
  globalData: {
    userInfo: null,
    systemInfo: null,
    tabBar: {
      "color": "#979795",
      "borderStyle":"black",
      "selectedColor": "#000000",
      "backgroundColor": "#ffffff",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "iconPath": "icon/icon_home.png",
          "selectedIconPath": "icon/icon_home_HL.png",
          "text": "首页"
        },
        {
          "pagePath": "/pages/shop/index",
          "iconPath": "icon/icon_active_not.png",
          "selectedIconPath": "icon/icon_active.png",
          "text": "商品"
        },
        {
          "pagePath": "/pages/filter/index",
          "iconPath": "icon/icon_release.png",
          "isSpecial": true,
          "text": "筛选"
        },
        {
          "pagePath": "/pages/shopcart/index",
          "iconPath": "icon/icon_active_not.png",
          "selectedIconPath": "icon/icon_active.png",
          "text": "购物车"
        },
        {
          "pagePath": "/pages/account/index",
          "iconPath": "icon/icon_mine.png",
          "selectedIconPath": "icon/icon_mine_HL.png",
          "text": "账户"
        }
      ]
    }
  }
})