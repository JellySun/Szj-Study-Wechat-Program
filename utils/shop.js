const content = {
  comprehensive:[// 综合的商品数据
    {id:1,name:'综合',type:1,active:'title-active',list:[
      {id:100,category:1,flag:true,name:'运动小脚裤',price:'75.00',amount:'769',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2019/155/278/10403872551_321203149.220x220.jpg'},
      {id:101,category:1,flag:false,name:'韩风外套',price:'75.00',amount:'1950',des:'2019新款百搭休闲女多款可选韩风外套',src:'https://cbu01.alicdn.com/img/ibank/2019/075/421/10386124570_21688717.170x170.jpg'},
      {id:102,category:1,flag:true,name:'夹克外套',price:'50.00',amount:'322',des:'2019春季新品连帽印花套头夹克外套',src:'https://cbu01.alicdn.com/img/ibank/2019/881/583/10411385188_475757154.310x310.jpg'},
      {id:104,category:1,flag:false,name:'休闲西装',price:'89.00',amount:'729',des:'2019春季新款男士休闲西装男韩版修身小西服时尚英伦单西青年外套',src:'https://cbu01.alicdn.com/img/ibank/2019/271/411/10412114172_693811189.310x310.jpg'},
      {id:105,category:1,flag:true,name:'欧美乞丐复古',price:'59.99',amount:'509',des:'2019宽松大破洞牛仔裤欧美乞丐复古做旧男士潮裤直筒修身九分哈伦',src:'https://cbu01.alicdn.com/img/ibank/2019/756/914/10425419657_963889447.310x310.jpg'},
      {id:106,category:1,flag:false,name:'圆领套头印花短袖',price:'109.00',amount:'1121',des:'包邮韩路休闲男装 2019夏季新品圆领套头印花短袖男士t恤上衣',src:'https://cbu01.alicdn.com/img/ibank/2019/835/414/10436414538_709365563.310x310.jpg'},
      {id:107,category:1,flag:false,name:'情侣皮带',price:'500.00',amount:'356',des:'厂家直销 女士腰带 针扣 男士皮带 通用  情侣皮带 宽 批发2006',src:'https://cbu01.alicdn.com/img/ibank/2017/959/359/4221953959_1421993766.310x310.jpg'},
      {id:108,category:1,flag:false,name:'潮鞋板鞋阿甘男鞋',price:'189.99',amount:'768',des:'夏季韩版男士运动休闲鞋跑步潮鞋板鞋阿甘男鞋子秋季透气气垫鞋子',src:'https://cbu01.alicdn.com/img/ibank/2017/043/959/4144959340_242872808.220x220xz.jpg'},
      {id:109,category:2,flag:false,name:'原味坚果',price:'35.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2016/209/632/3136236902_251078633.220x220xz.jpg'},
      {id:110,category:2,flag:true,name:'手工棒棒糖',price:'25.00',amount:'650',des:'圣诞节星空星球手工棒棒糖糖果零食男女友生日万圣情人节10支礼盒',src:'https://cbu01.alicdn.com/img/ibank/2018/728/561/9547165827_1788968618.220x220xz.jpg'},
      {id:111,category:2,flag:true,name:'威化饼干',price:'15.00',amount:'854',des:'意大利进口Loacker莱家粒粒装威化饼干110g/125g糕点零食休闲批发',src:'https://cbu01.alicdn.com/img/ibank/2018/866/262/8956262668_1475151554.220x220xz.jpg'},
      {id:112,category:2,flag:false,name:'金币巧克力',price:'68.00',amount:'325',des:'年货金币巧克力金元宝金币黑巧克力散装 烘焙巧克力500克儿童零食',src:'https://cbu01.alicdn.com/img/ibank/2018/746/805/9257508647_1678684759.220x220xz.jpg_.webp'}
    ]},
    {id:2,name:'热门零食',type:2,active:'', list:[
      {id:201,category:2,flag:false,name:'巧克力',price:'35.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/422/134/9407431224_514703803.220x220.jpg'},
      {id:202,category:2,flag:false,name:'饮料',price:'49.99',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/323/960/9288069323_924954154.220x220.jpg'},
      {id:203,category:2,flag:false,name:'三只松鼠',price:'78.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/071/496/9331694170_701725881.220x220.jpg'},
      {id:204,category:2,flag:false,name:'海鲜',price:'65.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2019/235/812/10317218532_1091940441.220x220.jpg'},
      {id:205,category:2,flag:false,name:'巧克力',price:'35.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/422/134/9407431224_514703803.220x220.jpg'},
      {id:206,category:2,flag:false,name:'饮料',price:'99.99',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/323/960/9288069323_924954154.220x220.jpg'},
      {id:207,category:2,flag:false,name:'三只松鼠',price:'25.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/071/496/9331694170_701725881.220x220.jpg'},
      {id:208,category:2,flag:false,name:'海鲜',price:'84.99',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2019/235/812/10317218532_1091940441.220x220.jpg'},
      {id:209,category:2,flag:false,name:'巧克力',price:'66.00',amount:'263',des:'北域珍奇 东北开口松子野生原味坚果零食特产 新货500g 厂家直销',src:'https://cbu01.alicdn.com/img/ibank/2018/422/134/9407431224_514703803.220x220.jpg'},
    ]},
    {id:3,name:'潮流服饰',type:3,active:'', list:[
      {id:301,category:1,flag:false,name:'衣服',price:'55.00',amount:'255',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2015/974/058/2433850479_190433778.220x220.jpg'},
      {id:302,category:1,flag:false,name:'裤子',price:'125.00',amount:'160',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2019/780/352/10408253087_695362126.220x220.jpg'},
      {id:303,category:1,flag:false,name:'帽子',price:'35.00',amount:'482',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2019/455/107/10336701554_1383699778.220x220.jpg'},
      {id:304,category:1,flag:false,name:'鞋子',price:'205.00',amount:'120',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2018/516/474/9839474615_425963487.220x220.jpg'},
      {id:305,category:1,flag:false,name:'衣服',price:'86.00',amount:'11',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2015/974/058/2433850479_190433778.220x220.jpg'},
      {id:306,category:1,flag:false,name:'裤子',price:'54.00',amount:'364',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2019/780/352/10408253087_695362126.220x220.jpg'},
      {id:307,category:1,flag:false,name:'帽子',price:'99.00',amount:'420',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2019/455/107/10336701554_1383699778.220x220.jpg'},
      {id:308,category:1,flag:false,name:'鞋子',price:'120.00',amount:'874',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2018/516/474/9839474615_425963487.220x220.jpg'},
      {id:309,category:1,flag:false,name:'鞋子',price:'160.00',amount:'588',des:'2019新款百搭运动小脚裤条纹系带男士九分裤',src:'https://cbu01.alicdn.com/img/ibank/2018/516/474/9839474615_425963487.220x220.jpg'}
    ]}
  ],
  recommand:[ // 推荐的商品数据
    {id:0,name:'',price:'',amount:'',des:'',src:''},
  ],
  netWorkShop:[
    {id:0,name: "购物网",active: "title-active", list:[
      {name:"淘宝",href:"https://www.taobao.com",icon: "//img.alicdn.com/tfs/TB1qpwlQXXXXXcCXXXXXXXXXXXX-256-256.png_60x60.jpg_.webp"},
      {name:"天猫",href:"https://www.tmall.com", icon: "//img.alicdn.com/tps/i3/TB1DGkJJFXXXXaZXFXX07tlTXXX-200-200.png_60x60.jpg_.webp"},
      {name:"京东",href:"https://www.jd.com",icon: "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/299c55e31d7f50ae4dc85faa90d6f445_121_121.jpg"},
      {name:"苏宁",href:"https://www.suning.com",icon: "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/84f7bb17623501accafe75da2ad57068_121_121.jpg"}
    ]},
    {id:1,name: "淘好货",active: "", list:[
      {name:"淘票票",href:"https://www.taopiaopiao.com",icon: "https://img.alicdn.com/tps/TB17c96OXXXXXaUXFXXXXXXXXXX-200-200.png_88x88xz.jpg"},
      {name:"唯品会",href:"http://www.vip.com/",icon: "https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=2446690469,1658130106&fm=58&bpow=1920&bpoh=1080"},
      {name:"拼多多",href:"https://www.pinduoduo.com",icon: "https://ss1.baidu.com/70cFfyinKgQFm2e88IuM_a/forum/pic/item/4afbfbedab64034f2bb76e46a6c379310a551d73.jpg"},
      {name:"聚划算",href:"https://ju.m.taobao.com/",icon: "https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=2635359046,3290293192&fm=58&bpow=800&bpoh=800"}
    ]}
  ],
  imgUrls:[
    "../../images/shop/img_banner1.png","../../images/shop/img_banner2.png",
    "../../images/shop/img_banner3.png","../../images/shop/img_banner4.png"
  ],
  getShopDetail(type, id){
    switch (type){
      case 1:
        break;
      default:
        break;
    }
  },
  shopType(id){
    switch (id){
      case "1":
        return "服装"
        break;
      case "2":
        return "食品"
        break;
      case "3":
        return "工具"
        break;
      default:
        return "其它"
        break;
    }
  }
}
module.exports = {
  content: content
}